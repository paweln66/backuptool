﻿namespace BackupService
{
	public class BackupRequest
	{
		public string Source { get; set; } 

		public string Destination { get; set; }
	}
}
