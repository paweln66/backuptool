﻿using System;
using System.Data.SQLite;

namespace BackupTool.ContentService
{
	public class FilesStorageInitializer : IFilesStorageInitializer
	{
		private readonly object initializerLock = new object();

		const string CreateSourceTableSql = "CREATE TABLE IF NOT EXISTS SourceFiles (" +
				"FileId INTEGER PRIMARY KEY," +
   				"FullPath TEXT NOT NULL," +
				"RelativePath TEXT NOT NULL," +
				"FileName TEXT NOT NULL," +
				"FileSizeBytes INTEGER DEFAULT 0" +
				"); ";

		const string CreateDestinationTableSql = "CREATE TABLE IF NOT EXISTS DestinationFiles (" +
				"FileId INTEGER PRIMARY KEY," +
   				"FullPath TEXT NOT NULL," +
				"RelativePath TEXT NOT NULL," +
				"FileName TEXT NOT NULL," +
				"FileSizeBytes INTEGER DEFAULT 0" +
				"); ";

		const string CreateSourceMissingFilesTableSql = "CREATE TABLE IF NOT EXISTS SourceMissingFiles (" +
				"Id INTEGER PRIMARY KEY," +
   				"SourceMissingFileId INTEGER NOT NULL," +
				"FOREIGN KEY (SourceMissingFileId) REFERENCES SourceFiles (FileId)" +
				"	ON DELETE CASCADE ON UPDATE NO ACTION" +
				"); ";

		const string CreateDestinationMissingFilesTableSql = "CREATE TABLE IF NOT EXISTS DestinationMissingFiles (" +
				"Id INTEGER PRIMARY KEY," +
				"DestinationMissingFileId INTEGER NOT NULL," +
				"FOREIGN KEY (DestinationMissingFileId) REFERENCES DestinationFiles (FileId)" +
				"	ON DELETE CASCADE ON UPDATE NO ACTION" +
				"); ";

		const string ConnectionString = "Data Source=BackupFilesStorage.db; Version = 3; New = True; Compress = True; ";

		private SQLiteConnection connection = null;

		public SQLiteConnection CreateConnection()
		{
			lock (initializerLock)
			{
				if (connection != null)
				{
					return connection;
				}

				// Create a new database connection:
				connection = new SQLiteConnection(ConnectionString);

				// Open the connection:
				try
				{
					connection.Open();
				}
				catch (Exception ex)
				{
					// TODO log
					throw;
				}
			}

			return connection;
		}

		public void CreateTables(SQLiteConnection conn)
		{
			try
			{
				SQLiteCommand connection;

				connection = conn.CreateCommand();
				connection.CommandText = CreateSourceTableSql;
				connection.ExecuteNonQuery();

				connection.CommandText = CreateDestinationTableSql;
				connection.ExecuteNonQuery();

				connection.CommandText = CreateSourceMissingFilesTableSql;
				connection.ExecuteNonQuery();

				connection.CommandText = CreateDestinationMissingFilesTableSql;
				connection.ExecuteNonQuery();
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex);
				throw;
			}
		}
	}
}
