﻿using BackupTool.ContentService.Progress;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace BackupService
{
	public interface IBackupService
	{
		Task RunBasicSyncAsync(BackupRequest request, IProgress<ProgressModel> progress, CancellationTokenSource cancellationToken);

		Task<DifferenceResponse> CollectDifferenceAsync(BackupRequest request, IProgress<ProgressModel> progress, CancellationTokenSource cancellationToken);

		Task<DifferenceResponse> GetSourceMissingFiles(IProgress<ProgressModel> progress, CancellationTokenSource cancellationToken);

		Task<DifferenceResponse> GetDestinationMissingFiles(IProgress<ProgressModel> progress, CancellationTokenSource cancellationToken);
	}
}
