﻿namespace BackupMenu
{
	partial class BackupMenu
	{
		/// <summary>
		///  Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		///  Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		///  Required method for Designer support - do not modify
		///  the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.sourcePath = new System.Windows.Forms.TextBox();
			this.destinationPath = new System.Windows.Forms.TextBox();
			this.SourceBtn = new System.Windows.Forms.Button();
			this.DestinationBtn = new System.Windows.Forms.Button();
			this.SyncBtn = new System.Windows.Forms.Button();
			this.DiffBtn = new System.Windows.Forms.Button();
			this.progressTextBox = new System.Windows.Forms.TextBox();
			this.CancelBtn = new System.Windows.Forms.Button();
			this.findDuplicatesBtn = new System.Windows.Forms.Button();
			this.sourceTreeView = new System.Windows.Forms.TreeView();
			this.destTreeView = new System.Windows.Forms.TreeView();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.progressBar = new System.Windows.Forms.ProgressBar();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(16, 15);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(112, 15);
			this.label1.TabIndex = 0;
			this.label1.Text = "Source (copy from):";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(7, 54);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(121, 15);
			this.label2.TabIndex = 1;
			this.label2.Text = "Destination (copy to):";
			// 
			// sourcePath
			// 
			this.sourcePath.Location = new System.Drawing.Point(134, 12);
			this.sourcePath.Name = "sourcePath";
			this.sourcePath.Size = new System.Drawing.Size(559, 23);
			this.sourcePath.TabIndex = 2;
			// 
			// destinationPath
			// 
			this.destinationPath.Location = new System.Drawing.Point(134, 51);
			this.destinationPath.Name = "destinationPath";
			this.destinationPath.Size = new System.Drawing.Size(559, 23);
			this.destinationPath.TabIndex = 3;
			// 
			// SourceBtn
			// 
			this.SourceBtn.Location = new System.Drawing.Point(699, 11);
			this.SourceBtn.Name = "SourceBtn";
			this.SourceBtn.Size = new System.Drawing.Size(75, 23);
			this.SourceBtn.TabIndex = 4;
			this.SourceBtn.Text = "Source";
			this.SourceBtn.UseVisualStyleBackColor = true;
			this.SourceBtn.Click += new System.EventHandler(this.SourceBtn_Click);
			// 
			// DestinationBtn
			// 
			this.DestinationBtn.Location = new System.Drawing.Point(699, 52);
			this.DestinationBtn.Name = "DestinationBtn";
			this.DestinationBtn.Size = new System.Drawing.Size(75, 23);
			this.DestinationBtn.TabIndex = 5;
			this.DestinationBtn.Text = "Destination";
			this.DestinationBtn.UseVisualStyleBackColor = true;
			this.DestinationBtn.Click += new System.EventHandler(this.DestinationBtn_Click);
			// 
			// SyncBtn
			// 
			this.SyncBtn.Location = new System.Drawing.Point(780, 172);
			this.SyncBtn.Name = "SyncBtn";
			this.SyncBtn.Size = new System.Drawing.Size(75, 46);
			this.SyncBtn.TabIndex = 6;
			this.SyncBtn.Text = "Sync";
			this.SyncBtn.UseVisualStyleBackColor = true;
			this.SyncBtn.Click += new System.EventHandler(this.SyncBtn_Click);
			// 
			// DiffBtn
			// 
			this.DiffBtn.Location = new System.Drawing.Point(780, 28);
			this.DiffBtn.Name = "DiffBtn";
			this.DiffBtn.Size = new System.Drawing.Size(75, 47);
			this.DiffBtn.TabIndex = 7;
			this.DiffBtn.Text = "Get Diff";
			this.DiffBtn.UseVisualStyleBackColor = true;
			this.DiffBtn.Click += new System.EventHandler(this.DiffBtn_Click);
			// 
			// progressTextBox
			// 
			this.progressTextBox.Location = new System.Drawing.Point(7, 503);
			this.progressTextBox.Multiline = true;
			this.progressTextBox.Name = "progressTextBox";
			this.progressTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
			this.progressTextBox.Size = new System.Drawing.Size(767, 148);
			this.progressTextBox.TabIndex = 8;
			// 
			// CancelBtn
			// 
			this.CancelBtn.Location = new System.Drawing.Point(780, 234);
			this.CancelBtn.Name = "CancelBtn";
			this.CancelBtn.Size = new System.Drawing.Size(75, 46);
			this.CancelBtn.TabIndex = 9;
			this.CancelBtn.Text = "Cancel";
			this.CancelBtn.UseVisualStyleBackColor = true;
			// 
			// findDuplicatesBtn
			// 
			this.findDuplicatesBtn.Location = new System.Drawing.Point(780, 81);
			this.findDuplicatesBtn.Name = "findDuplicatesBtn";
			this.findDuplicatesBtn.Size = new System.Drawing.Size(75, 46);
			this.findDuplicatesBtn.TabIndex = 10;
			this.findDuplicatesBtn.Text = "Find Duplicates";
			this.findDuplicatesBtn.UseVisualStyleBackColor = true;
			// 
			// sourceTreeView
			// 
			this.sourceTreeView.Location = new System.Drawing.Point(7, 115);
			this.sourceTreeView.Name = "sourceTreeView";
			this.sourceTreeView.Size = new System.Drawing.Size(375, 378);
			this.sourceTreeView.TabIndex = 11;
			// 
			// destTreeView
			// 
			this.destTreeView.Location = new System.Drawing.Point(399, 116);
			this.destTreeView.Name = "destTreeView";
			this.destTreeView.Size = new System.Drawing.Size(375, 377);
			this.destTreeView.TabIndex = 12;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(7, 97);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(185, 15);
			this.label3.TabIndex = 13;
			this.label3.Text = "Missing source files in destination";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(399, 97);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(185, 15);
			this.label4.TabIndex = 14;
			this.label4.Text = "Missing destination files in source";
			// 
			// progressBar
			// 
			this.progressBar.Location = new System.Drawing.Point(7, 657);
			this.progressBar.Name = "progressBar";
			this.progressBar.Size = new System.Drawing.Size(767, 23);
			this.progressBar.TabIndex = 15;
			// 
			// BackupMenu
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(871, 690);
			this.Controls.Add(this.progressBar);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.destTreeView);
			this.Controls.Add(this.sourceTreeView);
			this.Controls.Add(this.findDuplicatesBtn);
			this.Controls.Add(this.CancelBtn);
			this.Controls.Add(this.progressTextBox);
			this.Controls.Add(this.DiffBtn);
			this.Controls.Add(this.SyncBtn);
			this.Controls.Add(this.DestinationBtn);
			this.Controls.Add(this.SourceBtn);
			this.Controls.Add(this.destinationPath);
			this.Controls.Add(this.sourcePath);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Name = "BackupMenu";
			this.Text = "BackupMenu";
			this.Load += new System.EventHandler(this.BackupMenu_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox sourcePath;
		private System.Windows.Forms.TextBox destinationPath;
		private System.Windows.Forms.Button SourceBtn;
		private System.Windows.Forms.Button DestinationBtn;
		private System.Windows.Forms.Button SyncBtn;
		private System.Windows.Forms.Button DiffBtn;
		private System.Windows.Forms.TextBox progressTextBox;
		private System.Windows.Forms.Button CancelBtn;
		private System.Windows.Forms.Button findDuplicatesBtn;
		private System.Windows.Forms.TreeView sourceTreeView;
		private System.Windows.Forms.TreeView destTreeView;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.ProgressBar progressBar;
	}
}

