﻿namespace BackupTool.ContentService.Progress
{
	public class ProgressModel
	{
		public int Processed { get; set; }
		public int Total { get; set; }
		public string Message { get; set; }

		public ProgressModel() { }

		public ProgressModel(int proccesed, int total, string message)
		{
			Message = message;
			Processed = proccesed;
			Total = total;
		}
	}
}
