﻿using BackupTool.Storage;
using System.Collections.Generic;

namespace BackupTool.UI
{
	public class MissingFiles
	{
		public List<FileDto> SourceFilesToSync { get; set; }

		public List<FileDto> DestinationFilesToSync { get; set; }
	}
}
