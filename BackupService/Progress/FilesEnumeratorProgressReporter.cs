﻿using System;

namespace BackupTool.ContentService.Progress
{
	public class FilesEnumeratorProgressReporter : IFilesEnumeratorProgressReporter
	{
		private readonly object progressLock = new object();
		private IProgress<ProgressModel> progress;
		private ProgressModel progressModel;

		public FilesEnumeratorProgressReporter(IProgress<ProgressModel> progress, ProgressModel progressModel)
		{
			this.progress = progress;
			this.progressModel = progressModel;
		}

		public void IncrementProgress()
		{
			lock (progressLock)
			{
				progressModel.Processed++;
				var message 
					= $"Enumerating and Collecting all files from source and destination directories in parallel... Status {progressModel.Processed} out of {progressModel.Total}";
				progressModel.Message = message;
				progress.Report(progressModel);
			}
		}
	}
}
