﻿using System.Collections.Generic;

namespace BackupTool.Storage
{
	public interface IFilesStorageService
	{
		void ClearAllTables();

		IEnumerable<IList<FileDto>> GetNewSourceFiles(int pageSize = 0);

		IEnumerable<IList<FileDto>> GetNewDestinationFiles(int pageSize = 0);

		void AddSourcePath(FileDto path);

		void AddDestinationPath(FileDto path);

		void PersistMissingSourceFiles(IList<FileDto> path);

		void PersistMissingDestinationFiles(IList<FileDto> path);

		List<FileDto> GetMissingSourceFiles();

		List<FileDto> GetMissingDestinationFiles();
	}
}
