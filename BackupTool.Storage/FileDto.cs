﻿namespace BackupTool.Storage
{
	public class FileDto
	{
		public int Id { get; set; }

		public string FullPath { get; set; }

		public string RelativePath { get; set; }

		public string FileName { get; set; }
		
		public long FileSizeBytes { get; set; }
	}
}