﻿using BackupService;
using BackupTool.Storage;

namespace BackupTool.ContentService.Factory
{
	public class BackupServiceFactory : IBackupServiceFactory
	{
		public IBackupService GetBackupService()
		{
			IFilesServive fileService = new FilesServive();
			IFilesStorageInitializer filesStorageInitializer = new FilesStorageInitializer();
			IFilesStorageRepository filesStorageRepository = new FilesStorageRepository(filesStorageInitializer);
			IFilesStorageService filesStorageService = new FilesStorageService(filesStorageRepository);

			return new BackupService(fileService, filesStorageService);
		}
	}
}
