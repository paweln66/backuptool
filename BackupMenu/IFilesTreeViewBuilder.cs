﻿using BackupTool.Storage;
using System.Collections.Generic;
using System.Windows.Forms;

namespace BackupTool.UI
{
	public interface IFilesTreeViewBuilder
	{
		public List<TreeNode> BuildFilesTreeView(List<FileDto> files);
	}
}
