﻿namespace BackupTool.ContentService.Progress
{
	public interface IFilesEnumeratorProgressReporter
	{
		void IncrementProgress();
	}
}
