﻿using BackupService;
using BackupTool.ContentService.Progress;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace BackupTool.UI
{
	public interface ITaskOrchestrator
	{
		Task RunDifferenceCollectionTaskAsync(BackupRequest request, IProgress<ProgressModel> progress, CancellationTokenSource cancellationToken);
		
		Task<MissingFiles> GetMissingFilesAsync(IProgress<ProgressModel> progress, CancellationTokenSource cancellationToken);
	}
}
