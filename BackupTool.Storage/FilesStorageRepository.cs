﻿using BackupTool.ContentService;
using System;
using System.Collections.Generic;
using System.Data.SQLite;

namespace BackupTool.Storage
{
	public class FilesStorageRepository : IFilesStorageRepository
	{
		private readonly object initializerLock = new object();
		private readonly IFilesStorageInitializer filesStorageInitializer;
		private bool filesStorageIsInitialized = false;
		private const string OuterSourceFilesSql = 
			"SELECT * FROM SourceFiles SF " +
			"LEFT JOIN DestinationFiles DF " +
			"ON SF.RelativePath = DF.RelativePath " +
			"WHERE DF.RelativePath IS NULL";

		private const string OuterDestinationFilesSql =
			"SELECT * FROM DestinationFiles DF " +
			"LEFT JOIN SourceFiles SF " +
			"ON DF.RelativePath = SF.RelativePath " +
			"WHERE SF.RelativePath IS NULL";

		private const string ReadMissingSourceFilesSql =
			"SELECT * FROM SourceFiles SF INNER JOIN SourceMissingFiles MF ON SF.FileId = MF.SourceMissingFileId";

		private const string ReadMissingDestinationFilesSql =
			"SELECT * FROM DestinationFiles DF INNER JOIN DestinationMissingFiles MF ON DF.FileId = MF.DestinationMissingFileId";

		private const string InsertSourceFileSql =
			"INSERT INTO SourceFiles(FullPath, RelativePath, FileName, FileSizeBytes) VALUES(@fullPath, @relativePath, @fileName, @fileSizeBytes)";
		
		private const string InsertDestinationFileSql =
			"INSERT INTO DestinationFiles(FullPath, RelativePath, FileName, FileSizeBytes) VALUES(@fullPath, @relativePath, @fileName, @fileSizeBytes)";

		private const string TruncateSourcePathTableSql =
			"DELETE FROM SourceFiles;";

		private const string TruncateDestinationPathTableSql =
			"DELETE FROM DestinationFiles;";

		private const string TruncateSourceMissingFilesTableSql =
			"DELETE FROM SourceMissingFiles;";

		private const string TruncateDestinationMissingFilesTableSql =
			"DELETE FROM DestinationMissingFiles;";

		private const string InsertSourceMissingFileSql =
			"INSERT INTO SourceMissingFiles(SourceMissingFileId) VALUES(@sourceMissingFileId)";

		private const string InsertDestinationMissingFileSql =
			"INSERT INTO DestinationMissingFiles(DestinationMissingFileId) VALUES(@destinationMissingFileId)";


		public FilesStorageRepository(IFilesStorageInitializer filesStorageInitializer)
		{
			this.filesStorageInitializer = filesStorageInitializer;
		}

		public void InitializeFilesStorage()
		{
			lock (initializerLock)
			{
				if (filesStorageIsInitialized)
				{
					return;
				}

				SQLiteConnection connection = filesStorageInitializer.CreateConnection();
				filesStorageInitializer.CreateTables(connection);

				filesStorageIsInitialized = true;
			}
		}

		public void AddDestinationPath(FileDto path)
		{
			SQLiteCommand command = GetSQLiteCommand();
			command.CommandText = InsertDestinationFileSql;

			command.Parameters.AddWithValue("@fullPath", path.FullPath);
			command.Parameters.AddWithValue("@relativePath", path.RelativePath);
			command.Parameters.AddWithValue("@fileName", path.FileName);
			command.Parameters.AddWithValue("@fileSizeBytes", path.FileSizeBytes);
			command.Prepare();

			command.ExecuteNonQuery();
		}

		public void AddSourcePath(FileDto path)
		{
			SQLiteCommand command = GetSQLiteCommand();
			command.CommandText = InsertSourceFileSql;

			command.Parameters.AddWithValue("@fullPath", path.FullPath);
			command.Parameters.AddWithValue("@relativePath", path.RelativePath);
			command.Parameters.AddWithValue("@fileName", path.FileName);
			command.Parameters.AddWithValue("@fileSizeBytes", path.FileSizeBytes);
			command.Prepare();

			command.ExecuteNonQuery();
		}

		public IEnumerable<IList<FileDto>> GetFilesWithSizeMismatch(int pageSize = 0)
		{
			SQLiteCommand command = GetSQLiteCommand();

			throw new System.NotImplementedException();
		}

		public void TruncateSourcePathTable()
		{
			SQLiteCommand command = GetSQLiteCommand();
			command.CommandText = TruncateSourcePathTableSql;

			command.ExecuteNonQuery();
		}

		public void TruncateDestinationPathTable()
		{
			SQLiteCommand command = GetSQLiteCommand();
			command.CommandText = TruncateDestinationPathTableSql;

			command.ExecuteNonQuery();
		}

		public IList<FileDto> ReadOuterSourceFiles()
		{
			return ReadFilesList(OuterSourceFilesSql);
		}

		public IList<FileDto> ReadOuterDestinationFiles()
		{
			return ReadFilesList(OuterDestinationFilesSql);
		}

		public void TruncateSourceMissingFilesTable()
		{
			SQLiteCommand command = GetSQLiteCommand();
			command.CommandText = TruncateSourceMissingFilesTableSql;

			command.ExecuteNonQuery();
		}

		public void TruncateDestinationMissingFilesTable()
		{
			SQLiteCommand command = GetSQLiteCommand();
			command.CommandText = TruncateDestinationMissingFilesTableSql;

			command.ExecuteNonQuery();
		}

		public void AddMissingSourceFile(FileDto file)
		{
			SQLiteCommand command = GetSQLiteCommand();
			command.CommandText = InsertSourceMissingFileSql;

			command.Parameters.AddWithValue("@sourceMissingFileId", file.Id);
			command.Prepare();

			command.ExecuteNonQuery();
		}

		public void AddMissingDestinationFile(FileDto file)
		{
			SQLiteCommand command = GetSQLiteCommand();
			command.CommandText = InsertDestinationMissingFileSql;

			command.Parameters.AddWithValue("@destinationMissingFileId", file.Id);
			command.Prepare();

			command.ExecuteNonQuery();
		}

		private SQLiteCommand GetSQLiteCommand()
		{
			InitializeFilesStorage();

			SQLiteConnection connection = filesStorageInitializer.CreateConnection();
			return connection.CreateCommand();
		}

		public List<FileDto> ReadMissingSourceFiles()
		{
			return ReadFilesList(ReadMissingSourceFilesSql);
		}

		public List<FileDto> ReadMissingDestinationFiles()
		{
			return ReadFilesList(ReadMissingDestinationFilesSql);
		}

		private List<FileDto> ReadFilesList(string sql)
		{
			try
			{
				var result = new List<FileDto>();
				SQLiteDataReader sqliteDatareader;
				SQLiteCommand command = GetSQLiteCommand();
				command.CommandText = sql;

				sqliteDatareader = command.ExecuteReader();
				while (sqliteDatareader.Read())
				{
					var filePath = new FileDto
					{
						Id = Convert.ToInt32(sqliteDatareader["FileId"]),
						FileName = Convert.ToString(sqliteDatareader["FileName"]),
						FullPath = Convert.ToString(sqliteDatareader["FullPath"]),
						RelativePath = Convert.ToString(sqliteDatareader["RelativePath"]),
						FileSizeBytes = Convert.ToInt64(sqliteDatareader["FileSizeBytes"]),
					};

					result.Add(filePath);
				}

				return result;
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex);
				throw;
			}
		}
	}
}
