﻿using BackupTool.Storage;
using System.Collections.Generic;
using System.Windows.Forms;

namespace BackupTool.UI
{
	public class FilesTreeViewBuilder : IFilesTreeViewBuilder
	{
		public List<TreeNode> BuildFilesTreeView(List<FileDto> files)
		{
			if (files.Count == 0)
			{
				return new List<TreeNode>();
			}

			var treeNodesList = new List<TreeNode>();
			var root = BuildFoldersStructureTree(files);

			AppendTreeViewNodes(treeNodesList, root);
			return treeNodesList;
		}

		private static void AppendTreeViewNodes(List<TreeNode> treeNodesList, Dictionary<string, object> root)
		{
			foreach (var node in root)
			{
				TreeNode treeNode = new TreeNode(node.Key);
				if (node.Value != null)
				{
					foreach (var n in (Dictionary<string, object>)node.Value)
					{
						var dirNode = new TreeNode(n.Key);
						if (n.Value == null)
						{
							treeNode.Nodes.Add(new TreeNode(n.Key));
						}
						else
						{
							treeNode.Nodes.Add(AppendTreeViewSubNode(dirNode, (Dictionary<string, object>)n.Value));
						}
					}						
				}
				treeNodesList.Add(treeNode);
			}
		}

		private static TreeNode AppendTreeViewSubNode(TreeNode dirNode, Dictionary<string, object> subDirsDict)
		{
			foreach (var node in subDirsDict)
			{
				TreeNode treeNode = new TreeNode(node.Key);
				if (node.Value != null)
				{
					foreach (var n in (Dictionary<string, object>)node.Value)
					{
						var subDirNode = new TreeNode(n.Key);
						if (n.Value == null)
						{
							treeNode.Nodes.Add(new TreeNode(n.Key));
						}
						else
						{
							treeNode.Nodes.Add(AppendTreeViewSubNode(subDirNode, (Dictionary<string, object>)n.Value));
						}
					}
				}
				dirNode.Nodes.Add(treeNode);
			}
			return dirNode;
		}

		private Dictionary<string, object> BuildFoldersStructureTree(List<FileDto> files)
		{
			var root = new Dictionary<string, object>();
			foreach (var file in files)
			{
				string[] pathsElems = file.RelativePath.Split("\\");

				AppendFoldersStructureElement(root, pathsElems, 1);
			}

			return root;
		}

		private static void AppendFoldersStructureElement(Dictionary<string, object> node, string[] pathsElems, int i)
		{
			if (i == pathsElems.Length - 1)	//Add files
			{
				node.TryAdd(pathsElems[i], null);
			}
			else
			{
				if (node.ContainsKey(pathsElems[i]))
				{
					AppendFoldersStructureElement((Dictionary<string, object>)node[pathsElems[i]], pathsElems, i + 1);
				}
				else
				{
					node.Add(pathsElems[i], new Dictionary<string, object>());
					AppendFoldersStructureElement((Dictionary<string, object>)node[pathsElems[i]], pathsElems, i + 1);
				}				
			}
		}
	}
}
