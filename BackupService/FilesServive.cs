﻿using BackupTool.ContentService.Progress;
using BackupTool.Storage;
using System;
using System.IO;
using System.Threading;

namespace BackupService
{
	public class FilesServive : IFilesServive
	{
		public void EnumarateAllFilesRecursivelyAsync(string rootPath, Action<FileDto> persistFilesAction, IFilesEnumeratorProgressReporter progressReporter, CancellationTokenSource cancellationToken)
		{
            GetFilesRecursive(rootPath, rootPath, persistFilesAction, progressReporter, cancellationToken);
        }
            
		private void GetFilesRecursive(string currentPath, string rootPath, Action<FileDto> persistFilesAction, IFilesEnumeratorProgressReporter progressReporter, CancellationTokenSource cancellationToken)
        {
            try
            {
                foreach (string subPath in Directory.GetDirectories(currentPath))
                {
                    if(cancellationToken.IsCancellationRequested)
					{
                        break;
					}
                    GetFilesRecursive(subPath, rootPath, persistFilesAction, progressReporter, cancellationToken);
                }
                foreach (var file in Directory.GetFiles(currentPath))
                {
                    if (cancellationToken.IsCancellationRequested)
                    {
                        break;
                    }
                    progressReporter.IncrementProgress();
                    PersistFile(file, rootPath, persistFilesAction);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw;
            }
        }

        private void PersistFile(string file, string rootPath, Action<FileDto> persistFilesAction)
		{
            var filePath = new FileDto
            {
                FullPath = file,
                RelativePath = file.Replace(rootPath, string.Empty),
                FileName = Path.GetFileName(file),
                FileSizeBytes = new FileInfo(file).Length,
            };

            persistFilesAction(filePath);
        }
    }
}
