﻿using System.Collections.Generic;

namespace BackupTool.Storage
{
	public interface IFilesStorageRepository
	{
		void InitializeFilesStorage();

		void AddSourcePath(FileDto path);

		void AddDestinationPath(FileDto path);

		void AddMissingSourceFile(FileDto file);

		void AddMissingDestinationFile(FileDto file);

		void TruncateSourcePathTable();
		
		void TruncateDestinationPathTable();

		void TruncateSourceMissingFilesTable();

		void TruncateDestinationMissingFilesTable();

		IList<FileDto> ReadOuterSourceFiles();

		IList<FileDto> ReadOuterDestinationFiles();

		List<FileDto> ReadMissingSourceFiles();

		List<FileDto> ReadMissingDestinationFiles();

		IEnumerable<IList<FileDto>> GetFilesWithSizeMismatch(int pageSize = 0);
	}
}
