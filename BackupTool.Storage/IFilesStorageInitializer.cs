﻿using System.Data.SQLite;

namespace BackupTool.ContentService
{
	public interface IFilesStorageInitializer
	{
		SQLiteConnection CreateConnection();

		void CreateTables(SQLiteConnection conn);
	}
}
