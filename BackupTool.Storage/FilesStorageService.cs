﻿using System;
using System.Collections.Generic;

namespace BackupTool.Storage
{
	public class FilesStorageService : IFilesStorageService
	{
		private readonly IFilesStorageRepository filesStorageRepository;

		public FilesStorageService(IFilesStorageRepository filesStorageRepository)
		{
			this.filesStorageRepository = filesStorageRepository;
		}

		public void AddDestinationPath(FileDto path)
		{
			filesStorageRepository.AddDestinationPath(path);
		}

		public void PersistMissingDestinationFiles(IList<FileDto> files)
		{
			foreach(var file in files)
			{
				filesStorageRepository.AddMissingDestinationFile(file);
			}			
		}

		public void PersistMissingSourceFiles(IList<FileDto> files)
		{
			foreach (var file in files)
			{
				filesStorageRepository.AddMissingSourceFile(file);
			}
		}

		public void AddSourcePath(FileDto path)
		{
			filesStorageRepository.AddSourcePath(path);
		}

		public void ClearAllTables()
		{
			try
			{
				filesStorageRepository.TruncateSourceMissingFilesTable();
				filesStorageRepository.TruncateDestinationMissingFilesTable();

				filesStorageRepository.TruncateSourcePathTable();
				filesStorageRepository.TruncateDestinationPathTable();
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex);
				throw;
			}
		}

		public IEnumerable<IList<FileDto>> GetNewSourceFiles(int pageSize)
		{
			yield return filesStorageRepository.ReadOuterSourceFiles();
		}

		public IEnumerable<IList<FileDto>> GetNewDestinationFiles(int pageSize = 0)
		{
			yield return filesStorageRepository.ReadOuterDestinationFiles();
		}

		public List<FileDto> GetMissingSourceFiles()
		{
			return filesStorageRepository.ReadMissingSourceFiles();
		}

		public List<FileDto> GetMissingDestinationFiles()
		{
			return filesStorageRepository.ReadMissingDestinationFiles();
		}
	}
}
