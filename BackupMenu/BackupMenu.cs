﻿using BackupService;
using BackupTool.ContentService.Progress;
using BackupTool.ContentService.Factory;
using BackupTool.UI;
using Microsoft.VisualStudio.Threading;
using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BackupMenu
{
	public partial class BackupMenu : Form
	{
		private readonly ITaskOrchestrator taskOrchestrator;
		private readonly IFilesTreeViewBuilder filesTreeViewBuilder;
		readonly CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();

		//https://thomasfreudenberg.com/archive/2019/01/28/async-await-in-desktop-applications/
		private readonly JoinableTaskFactory _joinableTaskFactory;

		public BackupMenu()
		{
			InitializeComponent();

			BackupServiceFactory backupServiceFactory = new BackupServiceFactory();
			IBackupService backupService = backupServiceFactory.GetBackupService();
			taskOrchestrator = new TaskOrchestrator(backupService);
			filesTreeViewBuilder = new FilesTreeViewBuilder();

			_joinableTaskFactory = new JoinableTaskFactory(new JoinableTaskContext()); //TODO
		}

		private void SourceBtn_Click(object sender, EventArgs e)
		{
			using (var fbd = new FolderBrowserDialog())
			{
				DialogResult result = fbd.ShowDialog();

				if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
				{
					sourcePath.Text = fbd.SelectedPath;
				}
				else if (result == DialogResult.Cancel)
				{
					sourcePath.Text = string.Empty;
				}
			}
		}

		private void DestinationBtn_Click(object sender, EventArgs e)
		{
			using (var fbd = new FolderBrowserDialog())
			{
				DialogResult result = fbd.ShowDialog();

				if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
				{
					destinationPath.Text = fbd.SelectedPath;
				}
				else if (result == DialogResult.Cancel)
				{
					destinationPath.Text = string.Empty;
				}
			}
		}

		private void SyncBtn_Click(object sender, EventArgs e)
		{
			progressTextBox.Text += "SyncBtn_Click" + Environment.NewLine;
		}

		private async void DiffBtn_Click(object sender, EventArgs e)
		{
			try
			{
				await TaskScheduler.Default;

				var request = new BackupRequest
				{
					Source = sourcePath.Text,
					Destination = destinationPath.Text
				};
				var progress = new Progress<ProgressModel>(async progressModel => await UpdateProgressAsync(progressModel).ConfigureAwait(false)); //TODO Do something with this progress 	
				await taskOrchestrator.RunDifferenceCollectionTaskAsync(request, progress, cancellationTokenSource).ConfigureAwait(false);

				await _joinableTaskFactory.SwitchToMainThreadAsync();
				progressTextBox.Text += "RunDifferenceCollectionTaskAsync";

				await _joinableTaskFactory.SwitchToMainThreadAsync();
				var missingFiles = await taskOrchestrator.GetMissingFilesAsync(progress, cancellationTokenSource).ConfigureAwait(false);

				await _joinableTaskFactory.SwitchToMainThreadAsync();
				AssignTreeNodes(missingFiles);
			}
			catch (Exception ex)
			{
				DisplayErrorMessageBox(ex);
			}
		}

		private async Task UpdateProgressAsync(ProgressModel progressModel)
		{
			await _joinableTaskFactory.SwitchToMainThreadAsync();
			progressTextBox.Text = progressModel.Message + Environment.NewLine;
			int percentage = (int)((double)progressModel.Processed / progressModel.Total * 100);
			progressBar.Value = percentage;
		}

		private static void DisplayErrorMessageBox(Exception ex)
		{
			string message = ex.Message;
			string caption = "Error Occured";
			MessageBoxButtons buttons = MessageBoxButtons.OK;
			DialogResult result;

			result = MessageBox.Show(message, caption, buttons, MessageBoxIcon.Error);
			if (result == DialogResult.OK)
			{
				// Do nothing
			}
		}

		private void AssignTreeNodes(MissingFiles differenceResponse)
		{
			//foreach(var files in differenceResponse.SourceFilesToSync)
			//{
			//	textBox1.Text += files.RelativePath + Environment.NewLine;
			//}
			//textBox1.Text += Environment.NewLine + Environment.NewLine;
			//foreach (var files in differenceResponse.DestinationFilesToSync)
			//{
			//	textBox1.Text += files.RelativePath + Environment.NewLine;
			//}

			var srcTreeNodesList = filesTreeViewBuilder.BuildFilesTreeView(differenceResponse.SourceFilesToSync);
			foreach (var treeNode in srcTreeNodesList)
			{
				sourceTreeView.Nodes.Add(treeNode);
			}

			var destTreeNodesList = filesTreeViewBuilder.BuildFilesTreeView(differenceResponse.DestinationFilesToSync);
			foreach (var treeNode in destTreeNodesList)
			{
				destTreeView.Nodes.Add(treeNode);
			}
		}

		private void BackupMenu_Load(object sender, EventArgs e)
		{
			sourcePath.Text = @"D:\Development\Workspace\BackupTool sample data\root1";
			destinationPath.Text = @"D:\Development\Workspace\BackupTool sample data\root2";
		}
	}
}
