﻿using BackupService;
using BackupTool.ContentService.Progress;
using BackupTool.UI;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace BackupMenu
{
	public class TaskOrchestrator : ITaskOrchestrator
	{
		private readonly IBackupService backupService;

		public TaskOrchestrator(IBackupService backupService)
		{
			this.backupService = backupService;
		}

		public async Task RunDifferenceCollectionTaskAsync(BackupRequest request, IProgress<ProgressModel> progress, CancellationTokenSource cancellationToken)
		{
			var task = await Task.Factory.StartNew(
				() => backupService.CollectDifferenceAsync(request, progress, cancellationToken), 
				TaskCreationOptions.LongRunning);

			await task;
		}

		public async Task<MissingFiles> GetMissingFilesAsync(IProgress<ProgressModel> progress, CancellationTokenSource cancellationToken)
		{
			var taskSource = await Task.Factory.StartNew(
				() => backupService.GetSourceMissingFiles(progress, cancellationToken),
				TaskCreationOptions.LongRunning).ConfigureAwait(false);

			var taskDestination = await Task.Factory.StartNew(
				() => backupService.GetDestinationMissingFiles(progress, cancellationToken),
				TaskCreationOptions.LongRunning).ConfigureAwait(false);

			await Task.WhenAll(taskSource, taskDestination);

			return new MissingFiles
			{
				SourceFilesToSync = taskSource.Result.FilesToSync,
				DestinationFilesToSync = taskDestination.Result.FilesToSync
			};
		}
	}
}