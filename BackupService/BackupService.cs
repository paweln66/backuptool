﻿using BackupService;
using BackupTool.ContentService.Progress;
using BackupTool.Storage;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace BackupTool.ContentService
{
	public class BackupService : IBackupService
	{
		private readonly IFilesServive fileService;
		private readonly IFilesStorageService filesStorageService;

		public BackupService(IFilesServive fileService, IFilesStorageService filesStorageService)
		{
			this.fileService = fileService;
			this.filesStorageService = filesStorageService;
		}

		public async Task<DifferenceResponse> CollectDifferenceAsync(BackupRequest request, IProgress<ProgressModel> progress, CancellationTokenSource cancellationToken)
		{
			await EnumareFilesAsync(request, progress, cancellationToken);
			//progress.Report(GetProgressModel(20, "\tCompleted collecting all files!"));

			await PopulateMissingFiles(progress);
			progress.Report(GetProgressModel(50, "\tCompleted collecting missing files!"));

			//TODO
			//filesStorageRepository.GetFilesWithSizeMismatch();

			return new DifferenceResponse(); //TODO do not return response.
		}

		public Task<DifferenceResponse> GetSourceMissingFiles(IProgress<ProgressModel> progress, CancellationTokenSource cancellationToken)
		{
			progress.Report(GetProgressModel(60, "Getting missing source files..."));
			var response = new DifferenceResponse()
			{
				FilesToSync = filesStorageService.GetMissingSourceFiles()
			};
			progress.Report(GetProgressModel(70, "\tSource files retrieved!"));

			return Task.FromResult(response);
		}

		public Task<DifferenceResponse> GetDestinationMissingFiles(IProgress<ProgressModel> progress, CancellationTokenSource cancellationToken)
		{
			progress.Report(GetProgressModel(80, "Getting missing destination files..."));
			var response = new DifferenceResponse()
			{
				FilesToSync = filesStorageService.GetMissingDestinationFiles()
			};
			progress.Report(GetProgressModel(100, "\tDestination files retrieved!"));

			return Task.FromResult(response);
		}

		public async Task RunBasicSyncAsync(BackupRequest request, IProgress<ProgressModel> progress, CancellationTokenSource cancellationToken)
		{

		}

		private void CollectSourceMissingFiles()
		{
			var SourceFilesToSync = new List<FileDto>();
			// TODO Read files in loop for batches e.g. 10, and report to progress
			foreach (var diffBatch in filesStorageService.GetNewSourceFiles())
			{
				SourceFilesToSync.AddRange(diffBatch);
			}
			filesStorageService.PersistMissingSourceFiles(SourceFilesToSync);
		}

		private void CollectDestinationMissingFiles()
		{
			var DestinationFilesToSync = new List<FileDto>();
			// TODO Read files in loop for batches e.g. 10, and report to progress
			foreach (var diffBatch in filesStorageService.GetNewDestinationFiles())
			{
				DestinationFilesToSync.AddRange(diffBatch);
			}
			filesStorageService.PersistMissingDestinationFiles(DestinationFilesToSync);
		}

		private async Task EnumareFilesAsync(BackupRequest request, IProgress<ProgressModel> progress, CancellationTokenSource cancellationToken)
		{
			filesStorageService.ClearAllTables();

			FilesEnumeratorProgressReporter reporter = BuildReporter(request, progress);

			Task taskSource = Task.Run(
				() => fileService.EnumarateAllFilesRecursivelyAsync(request.Source, filesStorageService.AddSourcePath, reporter, cancellationToken));
			Task taskDestination = Task.Run(
				() => fileService.EnumarateAllFilesRecursivelyAsync(request.Destination, filesStorageService.AddDestinationPath, reporter, cancellationToken));

			await Task.WhenAll(taskSource, taskDestination);
		}

		private async Task PopulateMissingFiles(IProgress<ProgressModel> progress)
		{
			progress.Report(GetProgressModel(0, "Collecting missing source files in the destination directory and missing destination files in the source directory (in parallel)..."));
			
			Task taskSource = Task.Run(
				() => CollectSourceMissingFiles());
			Task taskDestination = Task.Run(
				() => CollectDestinationMissingFiles());

			await Task.WhenAll(taskSource, taskDestination);
		}

		private static FilesEnumeratorProgressReporter BuildReporter(BackupRequest request, IProgress<ProgressModel> progress)
		{
			int sourceCount = Directory.GetFiles(request.Source, "*", SearchOption.AllDirectories).Length;
			int destinationCount = Directory.GetFiles(request.Destination, "*", SearchOption.AllDirectories).Length;
			var total = sourceCount + destinationCount;
			var message = "Enumerating and Collecting all files from source and destination directories in parallel...";
			
			var progressModel = new ProgressModel(0, total, message);
			
			return new FilesEnumeratorProgressReporter(progress, progressModel);
		}

		private ProgressModel GetProgressModel(int proccesed, string message)
		{
			return new ProgressModel
			{
				Message = message,
				Processed = proccesed,
				Total = 100
			};
		}
	}
}
