﻿using BackupTool.ContentService.Progress;
using BackupTool.Storage;
using System;
using System.Threading;

namespace BackupService
{
	public interface IFilesServive
	{
		void EnumarateAllFilesRecursivelyAsync(string rootPath, Action<FileDto> action, IFilesEnumeratorProgressReporter progressReporter, CancellationTokenSource cancellationToken);
	}
}
