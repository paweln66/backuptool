﻿using BackupService;

namespace BackupTool.ContentService.Factory
{
	interface IBackupServiceFactory
	{
		IBackupService GetBackupService();
	}
}
